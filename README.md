# Xlsx_writer

Xlsx writer is an iOS project written in Swift and with Xcode12.5.

This is a sample project to demonstrate how to create xlsx file from an iOS application.

More here : [https://eorvain-app.medium.com/write-and-create-excel-files-in-swift-1b2dc282ad81](https://eorvain-app.medium.com/write-and-create-excel-files-in-swift-1b2dc282ad81)