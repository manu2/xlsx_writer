//
//  ViewController.swift
//  xlsx_writer
//
//  Created by Manu on 21/09/2021.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        ExportXlsxService().export()
    }
}

